void selectionSort(int a[], int size)
{
    int x = 0;
    for (int i = 0; i < size - 1; i++)
    {
        int smallestIndex = i;
        for (int j = i + 1; j < size; j++)
        {
            if (a[j] < a[smallestIndex]){
                smallestIndex = j;
            }

        }
        int temp = a[smallestIndex];
        a[smallestIndex] = a[i];
        a[i] = temp;
    }
}
