#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <iostream>

using namespace std;

void printArray(int a[], int length);


//Sorting Algorithms
void insertionSort(int a[], int size);
void selectionSort(int a[], int size);

#endif // MAIN_H_INCLUDED
