#include "main.h"

int main()
{
    //User enters size of array
    int size;
    cout << "Please Enter Size of Array: ";
    cin >> size;
    cout << endl;

    //initialize array
    int a[size];

    //populate array with user inputs
    //no checks in place for if input not int
    for (int i = 0; i < size; i++)
    {
        cout << "Please enter the " << i + 1 << " number of the array: ";
        cin >> a[i];
    }

    //call print array to output filled array
    printArray(a, size);

    /*
    ################################
    #######SORTING ALGORITHMS#######
    ################################
    */

    //Calling insertionSort
    //insertionSort(a,size);

    //Calling selectionSort
    selectionSort(a, size);

    //call print array to output sorted array
    printArray(a,size);

    return 0;
}

void printArray(int a[], int length)
{
    //Loop through array
    for (int i = 0; i < length; i++)
    {
        //print out numbers in a
        cout << a[i] << " ";
    }
    //add a line break at end
    cout << endl;
}


